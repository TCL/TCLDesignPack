# TCL Logos
This folder contains the logo in both the long form including the lab name as well as a short version without it. The logo design got a slight facelift: I moved the waves a bit away to make it more visually pleasing and the font of the lab name has been updated to Linux Libertine in demi bold. 

In the repository you find the logo in different formats for different usage scenarios. If you want to add the logo in PowerPoint please use the .emf file, for Latex use the PDF or eps. If you need to edit the file please use the SVG, but make shure that the font is installed (provided in the Fonts directory if needed). The *_all_vectors.* includes the lab name in vectorized form, allowing the use without the need of installing the font.

## Long Logo
### Color
![](logo/TCL/raster/tcl_logo_long_small.png)
- [For PowerPoint & Word](./logo/TCL/vector/tcl_logo_long_all_vectors.emf)
- [For pdflatex (PDF)](./logo/TCL/vector/tcl_logo_long_all_vectors.pdf)
- [For ancient latex (EPS)](./logo/TCL/vector/tcl_logo_long_all_vectors.eps)
- [SVG](./logo/vector/tcl_logo_long.svg)

### Black/White
![](logo/TCL/raster/tcl_logo_long_bw_small.png)
- [For PowerPoint & Word](./logo/TCL/vector/tcl_logo_long_all_vectors_bw.emf)
- [For pdflatex (PDF)](./logo/TCL/vector/tcl_logo_long_all_vectors_bw.pdf)
- [For ancient latex (EPS)](./logo/TCL/vector/tcl_logo_long_all_vectors_bw.eps)
- [SVG](./logo/TCL/vector/tcl_logo_long.svg)

## Short Logo
### Color
![](logo/TCL/raster/tcl_logo_short_small.png)
- [For PowerPoint & Word](./logo/TCL/vector/tcl_logo_short.emf)
- [For pdflatex](./logo/TCL/vector/tcl_logo_short.pdf)
- [For ancient latex](./logo/TCL/vector/tcl_logo_short.eps)
- [SVG](./logo/TCL/vector/tcl_logo_short.svg)

### Black/White
![](logo/TCL/raster/tcl_logo_short_bw_small.png)
- [For PowerPoint & Word](./logo/TCL/vector/tcl_logo_short_bw.emf)
- [For pdflatex](./logo/TCL/vector/tcl_logo_short_bw.pdf)
- [For ancient latex](./logo/TCL/vector/tcl_logo_short_bw.eps)
- [SVG](./logo/TCL/vector/tcl_logo_short_bw.svg)

# TCL Poster Template
This project contains a [LaTeX poster template](./poster/latex).

See the autobuilt example PDFs:
- [Example1](https://tclgit.epfl.ch/TCL/TCLDesignPack/builds/artifacts/master/file/poster/latex/example1.pdf?job=pdf).
- [Example2](https://tclgit.epfl.ch/TCL/TCLDesignPack/builds/artifacts/master/file/poster/latex/example2.pdf?job=pdf).
