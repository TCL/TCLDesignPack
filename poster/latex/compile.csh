#!/bin/csh
##################################################
### init
##################################################
set namefile=example1

##################################################
### main
##################################################

### compile
pdflatex ${namefile}
pdflatex ${namefile}
biber ${namefile}
pdflatex ${namefile}
pdflatex ${namefile} 

### remove log files
rm -f ${namefile}*.log
rm -f ${namefile}*.aux
rm -f ${namefile}*.bbl
rm -f ${namefile}*.blg
rm -f ${namefile}*.out
rm -f ${namefile}*.upa
rm -f ${namefile}*.upb
