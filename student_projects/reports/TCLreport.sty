%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TCLreport.sty
% Author: Andreas Toftegaard Kristensen <andreas.kristensen@epfl.ch>
% Date: 2022/05/13
%
% Custom packages for TCL student reports
% Written based on just keeping it all as simple as possible as no one in TCL
% really works with LaTeX on a regular basis and I usually forget how to write
% a package properly (these packages do not fully follow best-practices either)
%
% This package does not have any actual options, so \ProcessOptions\relax is not
% used.
%
% Many packages are disabled in this package, but you can enable them if you have the corresponding packages installed and need their functionality
%
% For information on writing packages see:
% https://en.wikibooks.org/wiki/LaTeX/Creating_Packages
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{TCLreport}[2022/05/13 v1.0 A4 page layout TCL style]
\pdfminorversion=7

%------------------------------------------------------------------------------
% Babel and languages
%------------------------------------------------------------------------------
\RequirePackage[english]{babel}

%------------------------------------------------------------------------------
% Fonts and encoding
%------------------------------------------------------------------------------
\newcommand\hmmax{0} % Fixes too many alphabets error https://tex.stackexchange.com/questions/3676/too-many-math-alphabets-error
\newcommand\bmmax{0}

\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc} % inputenc allows accented characters directly from the keyboard
\RequirePackage{lmodern}        % Latin Modern family of fonts.

%\RequirePackage{stfloats}     % Commands to control the presentation of floats
%\RequirePackage{fontawesome}  % web-related icons provided by the included font.
\RequirePackage{lipsum}       % to fill in with arbitrary text

\RequirePackage{csquotes}

%------------------------------------------------------------------------------
% Graphics
%------------------------------------------------------------------------------

\RequirePackage[usenames,dvipsnames]{xcolor} % red, green, blue, etc.
                                         % xcolor needs to be loaded before tikz

% !Important! If you load tikz, note that it loads the graphicx package, so you should outcomment the graphicx package below
% To get the correct options for the graphicx package use
% \PassOptionsToPackage{<options>}{graphicx}
% In our case
% \PassOptionsToPackage{final}{graphicx}
% or just remove it as tikz calls it

% \RequirePackage{tikz}

% http://tex.stackexchange.com/questions/42611/list-of-available-tikz-libraries-with-a-short-introduction
% \usetikzlibrary{
% arrows,             % Arrow tip library
% automata,           % Used for drawing "finite state automata and Turing Machines"
% backgrounds,        % Background library for background pictures
% calc,               % Make complex coordinate caculations
% chains,
% fit,                % Allows us to fit a square around multiple nodes
% intersections,      % Calculates intersections of paths
% snakes,
% patterns,
% matrix,             % Matrix Library
% mindmap,            % Mind map library
% shapes,             % Shape library, used to define shapes other than rectangle, circle and co-ordinate
% shadows,
% plotmarks,
% }

% Use circuitikz with siunitx if you wnat to draw electrical circuits
%\RequirePackage[siunitx]{circuitikz}
%
%\RequirePackage{pgfplots} % Used for plots
%\pgfplotsset{compat=newest}
%
%\RequirePackage{csvsimple}

%------------------------------------------------------------------------------
% Math
%------------------------------------------------------------------------------
\RequirePackage{mathtools}  % loads also amstext, amsbsy, amsopn (not amssymb)
\RequirePackage{amssymb}    % provides an extended symbol collection and replaces latexsym
\RequirePackage{bm}         % load after all math to give access to bold math

%\RequirePackage{ntheorem}       % Prefer over amsthm for theorems
%\RequirePackage{kbordermatrix}  % Easily make a matrix with row and column indicies shown
%
%\numberwithin{equation}{section}  % reset equation counters at start of each "section" and prefix numbers by section number
%\numberwithin{figure}{section}    % reset figure   counters at start of each "section" and prefix numbers by section number

%------------------------------------------------------------------------------
% Layout
%------------------------------------------------------------------------------

% See https://www.overleaf.com/learn/latex/Page_size_and_margins
% We base the page geometry on typearea package (replacements below using geometry etc.)
% https://tex.stackexchange.com/questions/182821/div-and-bcor-setting-in-koma-best-practices/183839#183839
%\RequirePackage{typearea}
% Result from report.log from running with \RequirePackage{typearea}
\paperwidth     = 597.50793pt
\textwidth      = 418.25555pt
\evensidemargin = 17.3562pt
\oddsidemargin  = 17.3562pt
\paperheight    = 845.04694pt
\textheight     = 595.80026pt
\topmargin      = -25.16531pt
\headheight     = 17.0pt
\headsep        = 20.40001pt
\topskip        = 11.0pt
\footskip       = 47.6pt
\baselineskip   = 13.6pt

% See also the following if you want a recommendation besides type area
% https://github.com/glederrey/EPFL_thesis_template/blob/master/head/settings_epfl_template.tex

% An a4paper is 210cmx297cm, we can hardcode the margins
%\RequirePackage[a4paper, top=3cm, bottom=3cm, left=2.5cm, right=2.5cm]{geometry}


% microtype gives many good lay-out/justification effects, see:
% http://texblog.net/latex-archive/latex-general/pdflatex-microtype/
% http://www.khirevich.com/latex/microtype/
\RequirePackage[
  activate={true,nocompatibility},  %  activate protrusion and expansion
  final,                            % final - enable microtype; use "draft" to disable
  tracking=true, kerning=true, spacing=true, % activate these techniques
  factor=1100,                      % add 10% to the protrusion amount (default is 1000)
  stretch=10, shrink=10,            % reduce stretchability/shrinkability (default is 20/20)
  babel
]{microtype}

% \widowpenalty = 4000        % help suppress widows,  default = 4,000 (?), from 0 to 10 000 (from 300 to 1 000 recommended, 10 000 not recommended)
% \clubpenalty  = 4000        % help suppress orphans, default = 4,000 (?), from 0 to 10 000 (from 300 to 1 000 recommended, 10 000 not recommended)

%------------------------------------------------------------------------------
% Graphicx and floats
%------------------------------------------------------------------------------

\RequirePackage[final]{graphicx} % \includegraphics for inserting figures
\RequirePackage{float}           % add [H] option for floats. Load before hyperref and algorithm
\RequirePackage[absolute]{textpos}

%------------------------------------------------------------------------------
% Figure and Subfigure Packages
%------------------------------------------------------------------------------

% \RequirePackage{caption}    % Allows for customizing captions
% \RequirePackage{subcaption} % For multiple sub-figures in one figure. Note that the subfigure and subfig packages are deprecated, use subcaption

%------------------------------------------------------------------------------
% Alignment and table packages
%------------------------------------------------------------------------------
% In general the rules are
% 1. Never, ever use vertical lines.
% 2. Never use double lines.

\RequirePackage{array} % offers more flexible column formatting; fixes to some spacing issues. An almost "must-use" package.

\RequirePackage{booktabs} % supports professional looking table, preferred
% \renewcommand{\arraystretch}{1.2}   % More space between rows:
% \begin{tabular}{@{}lll@{}}          % Trick to remove space to the vertical edges when making a table

% \RequirePackage{tabularx} % provides a column type X which expands to fill the specified width of the table

% \RequirePackage{rotating}
% \RequirePackage{multirow} % lets tabular material span multiple rows
% \RequirePackage{multicol}
% \RequirePackage{longtable}  % Allows for multi-page tables
% \RequirePackage{threeparttable}

% The following command allows you to use the \specialcell command
% to make forced multiline linebreak in rows in tables, see
% http://tex.stackexchange.com/questions/2441/how-to-add-a-forced-line-break-inside-a-table-cell

% \newcommand{\specialcell}[2][c]{%
  % \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}}

%------------------------------------------------------------------------------
% HYPERREF
%------------------------------------------------------------------------------

\RequirePackage{hyperref}
\hypersetup{
  colorlinks,
  linkcolor={red!50!black},
  citecolor={blue!50!black},
  urlcolor={blue!80!black}
}

\RequirePackage{url}

%------------------------------------------------------------------------------
% Algorithms
%------------------------------------------------------------------------------
% Typesetting algorithms
% See:
% https://en.wikibooks.org/wiki/LaTeX/Algorithms
% http://tex.stackexchange.com/questions/229355/algorithm-algorithmic-algorithmicx-algorithm2e-algpseudocode-confused

% Loads algpseudocode and algorithmicx together
% These packages are superior to algorithmic
%\RequirePackage{algorithm}
%\RequirePackage{algpseudocode}	% Should be loaded after hyperref

% \RequirePackage{enumitem} % Allows for greater control over lists
% https://tex.stackexchange.com/questions/519981/whats-the-difference-between-the-enumerate-and-enumitem-packages

%------------------------------------------------------------------------------
% Citation and gloassiers
%------------------------------------------------------------------------------

\RequirePackage[
  backend=bibtex,
  style=ieee,
]{biblatex}
\addbibresource{bibliography.bib}

%\RequirePackage{glossaries}
%\makeglossaries

%------------------------------------------------------------------------------
% Scientific/engineering packages
%------------------------------------------------------------------------------

\RequirePackage{siunitx} % Typeset with units \SI{1.0}{\newton\meter}

%------------------------------------------------------------------------------
% Misc. packages
%------------------------------------------------------------------------------
