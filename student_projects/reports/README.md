# TCL Report

The TCL report can be compiled with

```bash
make
```

That's it :) See the [Makefile](Makefile) for more commands like `make clean`.
